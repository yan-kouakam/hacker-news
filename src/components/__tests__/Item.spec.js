/* eslint-disable no-debugger */
import Item from '../Item.vue'
import { shallowMount } from '@vue/test-utils'

describe('Item.vue', () => {
  test('render item.url in a <a> with item.title as test', () => {
    const item = {
      title: 'A new on the hacker platform',
      url: 'www.fancyurl.test'
    }

    const wrapper = shallowMount(Item, {
      propsData: { item }
    })

    const link = wrapper.find('a')
    expect(link.text()).toContain(item.title)
    expect(link.attributes('href')).toEqual(item.url)
  })

  test('render the score', () => {
    const item = {
      score: 10
    }

    const wrapper = shallowMount(Item, {
      propsData: { item }
    })
    expect(wrapper.text()).toContain(item.score)
  })
})
