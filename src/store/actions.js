import { fetchListData } from '../api/api'

export const actions = {
  async fetchNewsList ({ commit }, { type }) {
    try {
      const items = await fetchListData(type)
      commit('setItems', { items })
    } catch (error) {
      console.log(error)
    }
  }
}
