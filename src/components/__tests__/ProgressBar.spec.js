/* eslint-disable no-debugger */
import { shallowMount } from '@vue/test-utils'
import ProgressBar from '../ProgressBar.vue'

describe('Progress Bar', () => {
  beforeEach(() => {
    jest.useFakeTimers()
  })

  test('Progress bar is initialy hidden and the width is 0%', () => {
    const wrapper = shallowMount(ProgressBar)
    expect(wrapper.classes()).toContain('hidden')
    let width = wrapper.find('div.hidden').element.style.width
    expect(width).toBe('0%')
  })

  test('shows the progress bar when the start() is call', () => {
    const wrapper = shallowMount(ProgressBar)
    wrapper.vm.start()
    let classes = wrapper.find('div').classes()
    let width = wrapper.find('div').element.style.width

    expect(classes).not.toContain('hidden')
    expect(width).toBe('0%')
  })

  test('hide the progress bar when the finish()', () => {
    const wrapper = shallowMount(ProgressBar)
    wrapper.vm.finish()
    const classes = wrapper.find('div').classes()
    const width = wrapper.find('div').element.style.width

    expect(width).toBe('100%')
    expect(classes).toContain('hidden')
  })

  test('The progress bar inscreases by 2% every 100ms', () => {
    const wrapper = shallowMount(ProgressBar)
    wrapper.vm.start()
    const style = wrapper.find('div').element.style
    expect(style.width).toBe('0%')

    jest.runTimersToTime(500)
    expect(style.width).toBe('10%')
    jest.runTimersToTime(1000)
    expect(style.width).toBe('30%')
    jest.runTimersToTime(3500)
    expect(style.width).toBe('100%')
    wrapper.vm.finish()
  })

  test('test that the clearInterval is call', () => {
    setInterval.mockReturnValue(150)
    const wrapper = shallowMount(ProgressBar)
    wrapper.vm.start()
    wrapper.vm.finish()
    const clearTimer = jest.spyOn(window, 'clearInterval')
    expect(clearTimer).toHaveBeenCalledWith(150)
  })
})
