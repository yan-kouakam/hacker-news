import store from '../store'
const { getters } = store
describe('Getters', () => {
  test('should test that the getter retrieve the correct data', () => {
    const items = Array(21).fill().map((v, i) => i)
    let state = {
      items
    }
    let res = getters.getDisplayItems(state)

    expect(res).toHaveLength(20)

    expect(res[0]).toBe(items[0])
  })

  test('should check the max page is correct', () => {
    const items = Array(29).fill().map((v, i) => i)
    let state = {
      items
    }

    const num = getters.getMaxPage(state)
    
    expect(num).toBe(2)
  })
})
