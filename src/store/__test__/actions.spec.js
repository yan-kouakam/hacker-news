import store from '../store'
import { fetchListData } from '../../api/api'
import flusPromises from 'flush-promises'

jest.mock('../../api/api.js')
const { actions } = store

describe('Action', () => {
  test('should update the action', async () => {
    expect.assertions(1)
    const context = {
      commit: jest.fn()
    }
    const type = 'top'
    const items = Array(53).fill().map((value, i) => i)
    fetchListData.mockImplementationOnce(callWith => {
      return callWith === type
        ? Promise.resolve(items) : Promise.resolve()
    })

    actions.fetchNewsList(context, { type: 'top' })
    await flusPromises()
    expect(context.commit).toHaveBeenCalledWith('setItems', { items })
  })
})
