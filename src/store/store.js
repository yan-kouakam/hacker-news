
import { actions } from './actions'
import { getters } from './getters'
import { mutations } from './mutations'

const state = {
  items: []
}

export default {
  state,
  mutations,
  actions,
  getters
}
