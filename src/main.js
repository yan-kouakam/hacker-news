import Vue from 'vue'
import App from './App'
import storeConfig from './store/store'
import ProgressBar from './components/ProgressBar.vue'

import Vuex from 'vuex'

Vue.use(Vuex)

const store = new Vuex.Store(storeConfig)

const bar = new Vue(ProgressBar).$mount()
Vue.prototype.$bar = bar

document.body.appendChild(bar.$el)
Vue.config.productionTip = false

new Vue({
  el: '#app',
  store,
  render: h => h(App)
})
