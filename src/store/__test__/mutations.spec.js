import store from '../store'
const { mutations } = store
describe('Mutation', () => {
  test('test that the state is update with the payload', () => {
    let state = {
      items: []
    }

    const items = [{}, {}, {}]

    mutations.setItems(state, { items })
    expect(state.items).toHaveLength(items.length)

    expect(state.items).toBe(items)
  })
})
