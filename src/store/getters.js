
export const getters = {
  getDisplayItems (state) {
    return state.items.slice(0, 20)
  },

  getMaxPage (state) { return Math.ceil(state.items.length / 20) }
}

