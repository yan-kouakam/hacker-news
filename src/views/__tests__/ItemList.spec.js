/* eslint-disable no-debugger */
import Vuex from 'vuex'
import { shallowMount, createLocalVue } from '@vue/test-utils'
import ItemList from '../ItemList.vue'
import Item from '../../components/Item.vue'
import flushPromises from 'flush-promises'
jest.mock('../../api/api.js')

const localVue = createLocalVue()
localVue.use(Vuex)

const createitems = () => {
  return Array(27).fill().map((val, ind) => { return { id: `id_${ind}`, name: `item_${ind}` } })
}
let store
let storeOptions
describe('Item list', () => {
  beforeEach(() => {
    storeOptions = {

      getters: {
        getDisplayItems: jest.fn()
      },

      actions: {
        fetchNewsList: jest.fn(() => Promise.resolve(createitems()))
      }
    }
    store = new Vuex.Store(storeOptions)
  })

  test('Call the start function of the progress bar when mounted', () => {
    const $bar = {
      start: jest.fn(),
      finish: () => {}
    }

    shallowMount(ItemList, { localVue,
      mocks: {
        $bar
      },
      store
    })

    expect($bar.start).toHaveBeenCalledTimes(1)
  })

  test('the dispatch method is called', async () => {
    expect.assertions(1)

    const $bar = {
      start: () => {},
      finish: jest.fn()
    }

    store.dispatch = jest.fn(() => Promise.resolve())

    shallowMount(ItemList, { localVue, mocks: { $bar }, store })
    await flushPromises()

    expect(store.dispatch).toHaveBeenCalledWith('fetchNewsList', { type: 'top' })
    // expect($bar.finish).toHaveBeenCalledTimes(1)
  })

  test('fetch data from fire base', async () => {
    expect.assertions(4)

    const $bar = {
      start: () => {},
      finish: () => {}
    }

    const items = [{ id: 1 }, { id: 2 }, { id: 3 }]

    // storeOptions.actions.fetchNewsList.mockResolvedValueOnce(items)
    storeOptions.getters.getDisplayItems.mockReturnValueOnce(items)
    const wrapper = shallowMount(ItemList, { mocks: { $bar }, localVue, store })
    await flushPromises()
    const Items = wrapper.findAll(Item)
    expect(Items).toHaveLength(Items.length)

    Items.wrappers.forEach((wrapper, i) => {
      const item = wrapper.vm.item
      expect(item).toEqual(items[i])
    })
  })
})
