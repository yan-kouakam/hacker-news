import { createLocalVue } from '@vue/test-utils'
import cloneDeep from 'lodash.clonedeep'
import flushPromises from 'flush-promises'
import Vuex from 'vuex'
import store from '../store'

import { fetchListData } from '../../api/api'
jest.mock('../../api/api')

const localVue = createLocalVue()
localVue.use(Vuex)

const createitems = () => {
  return Array(27).fill().map((val, ind) => { return { id: `id_${ind}`, name: `item_${ind}` } })
}

describe('Store configuration', () => {
  test('should dispatch the fetch data and update the state', async () => {
    expect.assertions(1)
    const storeClone = cloneDeep(store)
    const concreStore = new Vuex.Store(storeClone)
    const items = createitems()

    fetchListData.mockImplementationOnce(callWith => {
      return callWith === 'top' ? Promise.resolve(items) : Promise.resolve()
    })

    concreStore.dispatch('fetchNewsList', { type: 'top' })

    await flushPromises()

    expect(concreStore.getters.getDisplayItems).toEqual(items.slice(0, 20))
  })
})
